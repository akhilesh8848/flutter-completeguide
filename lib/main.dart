import 'package:flutter/material.dart';

import 'quiz.dart';
import 'result.dart';

//void main(){
  //runApp(MyApp());
//}

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    //throw UnimplementedError();
    return MyAppState();
  }
}

class MyAppState extends State<MyApp>{
  var _questionIndex=0;
  var _totalScore=0;

  void _resetQuiz(){
    setState(() {
      _questionIndex=0;
      _totalScore=0;
    });
  }

  void _answerQuestion(int score){
    _totalScore=_totalScore + score;
    setState(() {
      _questionIndex++;
    });
    print(_questionIndex);
  }
  @override
  Widget build(BuildContext context){
    final _questions=const [
      {'questionText':'What is your favorite color?', 'answers':[{'text':'Black','score': 10},{'text':'Red','score': 6},{'text':'Blue','score': 3},{'text':'White','score': 1}]},
      {'questionText':'What is your favorite animal?','answers':[{'text':'lion','score':6},{'text':'Tiger','score':2},{'text':'Cheetah','score':4},{'text':'Snake','score':8}]},
      {'questionText':'Who is your favorite cricketer?','answers':[{'text':'Virat','score':1},{'text':'Dhoni','score':9},{'text':'Sachin','score':2},{'text':'Gayle','score':3}]}
      ];
    return MaterialApp(home: Scaffold(
      appBar:AppBar(title:Text('My First App'),) ,
      body: _questionIndex < _questions.length
          ? Quiz(answerQuestion: _answerQuestion, questionIndex: _questionIndex, questions:_questions)
          : Result(_totalScore,_resetQuiz),
      ),
    );
  }
}